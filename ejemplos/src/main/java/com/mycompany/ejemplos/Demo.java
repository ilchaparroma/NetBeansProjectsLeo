/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejemplos;

import com.mycompany.ejemplos.factory.Dialog;
import com.mycompany.ejemplos.factory.HtmlDialog;
import com.mycompany.ejemplos.factory.WindowsDialog;

/**
 * EN: Demo class. Everything comes together here.
 *
 
 */
public class Demo {
    private static Dialog dialog;

    public static void main(String[] args) {
        configure();
        runBusinessLogic();
    }

    /**
     * EN: The concrete factory is usually chosen depending on configuration or
     * environment options.
     *

     */
    static void configure() {
        if (System.getProperty("os.name").equals("Windows 10")) {
            dialog = new WindowsDialog();
        } else {
            dialog = new HtmlDialog();
        }
    }

    /**
     * EN: All of the client code should work with factories and products
     * through abstract interfaces. This way it does not care which factory it
     * works with and what kind of product it returns.
     *

     */
    static void runBusinessLogic() {
        dialog.renderWindow();
    }
}
