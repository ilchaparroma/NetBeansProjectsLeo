
package com.mycompany.ejemplos.buttons;

/**
 * EN: Common interface for all buttons.
 *
 
 */
public interface Button {
    void render();
    void onClick();
}
