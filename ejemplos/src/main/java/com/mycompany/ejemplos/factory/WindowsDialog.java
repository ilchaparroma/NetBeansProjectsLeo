package com.mycompany.ejemplos.factory;

import com.mycompany.ejemplos.buttons.Button;
import com.mycompany.ejemplos.buttons.WindowsButton;


/**
 * EN: Windows Dialog will produce Windows buttons.
 *
 
 */
public class WindowsDialog extends Dialog {

    @Override
    public Button createButton() {
        return new WindowsButton();
    }
}
