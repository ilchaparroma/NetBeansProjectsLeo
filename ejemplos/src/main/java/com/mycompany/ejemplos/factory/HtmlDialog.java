package com.mycompany.ejemplos.factory;

import com.mycompany.ejemplos.buttons.Button;
import com.mycompany.ejemplos.buttons.HtmlButton;


/**
 * EN: HTML Dialog will produce HTML buttons.
 *
 
 */
public class HtmlDialog extends Dialog {

    @Override
    public Button createButton() {
        return new HtmlButton();
    }
}
