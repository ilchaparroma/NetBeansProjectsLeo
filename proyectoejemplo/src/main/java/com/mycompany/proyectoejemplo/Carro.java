/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyectoejemplo;

/**
 *
 * @author Leandro
 */
public class Carro {
    
    String marca;
    String modelo;
    int precio;
    
    //------------------------------------

    public Carro(String marca, String modelo, int precio) {
        this.marca = marca;
        this.modelo = modelo;
        this.precio = precio;
    }
    
    //-------------------------------------

    @Override
    public String toString() {
        return "Carro{" + "marca=" + marca + ", modelo=" + modelo + ", precio=" + precio + '}';
    }
    
    
    
    
}
